// <auto-generated />
namespace RentalMoviesTask.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class initMovies : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(initMovies));
        
        string IMigrationMetadata.Id
        {
            get { return "201904181147372_initMovies"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
