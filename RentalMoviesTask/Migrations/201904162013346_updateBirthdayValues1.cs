namespace RentalMoviesTask.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateBirthdayValues1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Customers", "birthDate", c => c.DateTime());
            AlterColumn("dbo.MembershipTypes", "Name", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MembershipTypes", "Name", c => c.String());
            AlterColumn("dbo.Customers", "birthDate", c => c.DateTime(nullable: false));
        }
    }
}
