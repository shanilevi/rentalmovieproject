﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RentalMoviesTask.Startup))]
namespace RentalMoviesTask
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
