﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RentalMoviesTask.Models;
using System.ComponentModel.DataAnnotations;

namespace RentalMoviesTask.ViewModels
{
    public class FormMovieViewModel
    {
        public int? Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Display(Name = "Genre")]
        [Required]
        public byte? GenreId { get; set; }

        [Display(Name = "Number in stock")]
        [Required]
        [Range(1, 20)]
        public int? NumberInStock { get; set; }

        [Display(Name = "Released Date")]
        [Required]
        public DateTime? ReleasedDate { get; set; }

        public IEnumerable<Genre> Genres { get; set; }

        public String Title
        {
            get
            {
                return (Id != 0)? "Edit Movie" : "New Movie";
            }
        }
        public FormMovieViewModel()
        {
            Id = 0;
        }
        public FormMovieViewModel(Movie movie)
        {
            Id = movie.Id;
            Name = movie.Name;
            ReleasedDate = movie.ReleasedDate;
            NumberInStock = movie.NumberInStock;
            GenreId = movie.GenreId;
        }
    }
}